package com.shafic.test.steps;

import com.shafic.test.pages.HomePage;
import com.shafic.test.pages.ImdbSignInPage;
import com.shafic.test.pages.RegistrationPage;
import com.shafic.test.pages.SignUpPage;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.sql.Timestamp;
import java.util.HashMap;

public class SignUpScenariosSteps {

    @Steps
    UserSteps user;
    HomePage homePage;
    RegistrationPage regpage;
    SignUpPage signUpPage;
    ImdbSignInPage imdbSignInPage;

    HashMap map= new HashMap();


    @Given("^I have user details for sign up (.*) (.*) (.*)$")
    public void iHaveUserDetailsForSignUpEmailPasswordName(String Email,String password,String Name) throws Throwable {
        map.put("email",Email+getRandomNumber()+"@gmail.com");
        map.put("password",password);
        map.put("name", Name);

        user.opens_home_page();
    }

    @When("^I input the user details and submit$")
    public void iInputTheUserDetailsAndSubmit() throws Throwable {
        user.navigate_to_imdb_registration();
        user.fill_registration_form_and_submit(map);
    }

    @Then("^I should be able to see my account page$")
    public void iShouldBeAbleToSeeMyAccountPage() throws Throwable {
        user.verify_account_details_and_logout(map);
    }


   public static String getRandomNumber() {
       Timestamp timestamp = new Timestamp(System.currentTimeMillis());
       long randomLong=timestamp.getTime();
       return String.valueOf(randomLong);
   }


}

package com.shafic.test.steps;

import com.shafic.test.pages.*;
import net.thucydides.core.annotations.Step;


import java.util.HashMap;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsCollectionContaining.hasItems;

public class UserSteps {
    HomePage homePage;
    MostWatchTVShowPage popularShow;
    RegistrationPage registrationPage;
    SignUpPage signUpPage;
    ImdbSignInPage imdbSignInPage;

    CirclesSignInPage circlesSignInPage;
    FacebookLoginPage facebookLoginPage;
    FacebookWallPage facebookWallPage;

    @Step
    public void opens_home_page() {
       // homePage.open();
    }

    @Step
    public void opens_circles_page() {
        circlesSignInPage.open();
    }

    @Step
    public void opens_facebook_web_page() {
        facebookLoginPage.open();
      //  getDriver().manage().window().maximize();
    }

    @Step
    public void navigate_to_mostPopularTVShows() {
        //homePage.clickOnMostPopularTvShows(homePage.getDriver());
     //   homePage.accessMoivesAndTvShowsMenu(homePage.getDriver());
      //  homePage.accessMoivesAndTvShowsMenu();
    }

    @Step
    public void find_tvshows_In_mostPopularTVShows(String show) {

        assertThat(popularShow.getShowData(), hasItems(show));
    }

    @Step
    public void fill_registration_form_and_submit(HashMap map) {

       signUpPage.fillAndSubmit(map);
    }

    @Step
    public void navigate_to_imdb_registration() {

      //  homePage.clickOtherSignInOptionsLink();
        registrationPage.clickCreateANewAccountLink();
    }

    @Step
    public void verify_account_details_and_logout(HashMap map) {

       /* homePage.verifyAccountDetails(map.get("name").toString());
        homePage.clickLogOutLink();*/
    }

    @Step
    public void login_to_cireclesPage(HashMap map) {

        circlesSignInPage.fillAndSubmit(map);
        System.out.println("test"+circlesSignInPage.getTitle());


    }

    @Step
    public void login_to_Facebook_WebPage(HashMap map) {

        facebookLoginPage.fillAndSubmit(map);

    }

    @Step
    public void Facebook_WebPage_navigate_newsFeed() {


        facebookWallPage.clickNewsFeedLink();

    }

    @Step
    public void Facebook_WebPage_ComposePost() {

        facebookWallPage.clickComposePostLink();

    }

}

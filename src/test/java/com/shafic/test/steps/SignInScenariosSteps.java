package com.shafic.test.steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SignInScenariosSteps {

    @Steps
    UserSteps user;

    HashMap map= new HashMap();

    @Given("^I have user details for sign in (.*) (.*)$")
    public void iHaveUserDetailsForSignInEmailPassword(String Email,String password) throws Throwable {

        map.put("email",Email);
        map.put("password",password);

        user.opens_circles_page();

    }


    @When("^I input the given user details and submit$")
    public void iInputTheGivenUserDetailsAndSubmit() throws Throwable {
        user.login_to_cireclesPage(map);
    }

    @Then("^I should be able to login to my account page$")
    public void iShouldBeAbleToLoginToMyAccountPage() throws Throwable {
        assertEquals(user.circlesSignInPage.getTitle(),"Unlimit your telco. Now.");
    }

    @Given("^I have facebook user details for sign in (.*) (.*)$")
    public void iHaveFacebookUserDetailsForSignInEmailPassword(String UserId,String password) throws Throwable {
        map.put("UserId",UserId);
        map.put("password",password);
        user.opens_facebook_web_page();

        
    }

    @When("^I input the given facebook user details and submit$")
    public void iInputTheGivenFacebookUserDetailsAndSubmit() throws Throwable {
        user.login_to_Facebook_WebPage(map);
        user.Facebook_WebPage_navigate_newsFeed();
        user.Facebook_WebPage_ComposePost();
    }

    @Then("^I should be able to see my wall$")
    public void iShouldBeAbleToSeeMyWall() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @When("^I input the given user details and submit  sd$")
    public void iInputTheGivenUserDetailsAndSubmitSd() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}

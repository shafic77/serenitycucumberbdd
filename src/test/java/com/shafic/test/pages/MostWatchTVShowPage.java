package com.shafic.test.pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import java.util.*;

import static org.junit.Assert.assertTrue;

public class MostWatchTVShowPage extends PageObject {

    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(xpath = "//td[@class='titleColumn']/a[text()]")
    @CacheLookup
    private List<WebElement> top100shows;

    /**
     * Get show data.
     *
     * @return List of most watched show data.
     */

    public List<String> getShowData() {

        ListIterator<WebElement> litr = null;
        List resultData = new ArrayList();
        litr=top100shows.listIterator();
        while(litr.hasNext())
        {
            resultData.add(litr.next().getText().trim());

        }
        return resultData;

    }

    public MostWatchTVShowPage() {
    }

    public MostWatchTVShowPage(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public MostWatchTVShowPage(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public MostWatchTVShowPage(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }

}

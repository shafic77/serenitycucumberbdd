package com.shafic.test.pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Map;

public class FacebookWallPage extends PageObject {

    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "a[title='News Feed']")
    @CacheLookup
    private WebElement newsFeed;

   /* @FindBy(css = "#js_1jf > div._i-o._2j7c > div > div.clearfix._ikh > div._4bl9 > div > div > div")
    @CacheLookup
    private WebElement makePost;*/

    //@FindBy(css = "//*[@id=\"js_1jf\"]/div[1]/div/div[1]/div[2]/div/div/div/div/div/div[2]/div/div/div/div")
    @FindBy(xpath = "//span[text()='Compose Post']")
    @CacheLookup
    private WebElement composePost;

    //@FindBy(xpath = "//div[starts-with(text(), \"What's on your mind\")]")
    //@FindBy(xpath = "//span[@data-offset-key = 'dsu5-0-0']/br")
    //@FindBy(xpath = "//div[contains(@class,'_5rpu')]")
    @FindBy(xpath = "//div[@role='textbox']")
    private WebElement postMsg;

    @FindBy(xpath = "//div[@data-offset-key='dsu5-0-0']")
    private WebElement postMsgBox;

    //@FindBy(css = "//*[@id=\"js_1jf\"]/div[2]/div[3]/div/div[2]/div/span[2]/button")
    @FindBy(xpath = "//button/span[text()='Post']")
    private WebElement post;

    /**
     * Click on Compose Post Link.
     *
     * @return the FacebookWallPage class instance.
     */
    public FacebookWallPage clickComposePostLink() {
       // makePost.click();
        composePost.click();
        //postMsgBox.click();
        try {
            Thread.sleep(4000);
        }catch (Exception e){
            System.out.println(e);
        }
        //new WebDriverWait(driver, 5).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@role='textbox']")));

        System.out.println(postMsg.isDisplayed());
        System.out.println(postMsg.isEnabled());
        postMsg.sendKeys("Hello12345");
        post.click();
        return this;
    }

    /**
     * Click on News Feed Link.
     *
     * @return the FacebookWallPage class instance.
     */
    public FacebookWallPage clickNewsFeedLink() {
        newsFeed.click();
        return this;
    }

    public FacebookWallPage() {
    }

    public FacebookWallPage(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public FacebookWallPage(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public FacebookWallPage(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }

}

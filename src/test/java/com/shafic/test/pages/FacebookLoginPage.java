package com.shafic.test.pages;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.HashMap;
import java.util.Map;


@DefaultUrl("https://www.facebook.com/")
public class FacebookLoginPage extends PageObject {

    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(id = "email")
    @CacheLookup
    private WebElement emailOrPhone;

    @FindBy(id = "pass")
    @CacheLookup
    private WebElement password;

    //@FindBy(id = "u_0_2")
    @FindBy(xpath = "//label[@id='loginbutton']/input")
    @CacheLookup
    private WebElement logIn1;

    /**
     * Click on Log In Link.
     *
     * @return the FacebookLoginPage class instance.
     */
    public FacebookLoginPage clickLogInLink() {

        logIn1.click();
        return this;
    }

    /**
     * Set value to Email Or Phone Email field.
     *
     * @return the FacebookLoginPage class instance.
     */
    public FacebookLoginPage setEmailOrPhoneEmailField(String emailOrPhoneValue) {
        emailOrPhone.sendKeys(emailOrPhoneValue);
        return this;
    }

    /**
     * Set value to Password Password field.
     *
     * @return the FacebookLoginPage class instance.
     */
    public FacebookLoginPage setPasswordPasswordField(String passwordValue) {
        password.sendKeys(passwordValue);
        return this;
    }

    /**
     * Fill every fields in the page.
     *
     * @return the FacebookLoginPage class instance.
     */
    public FacebookLoginPage fillAndSubmit(HashMap map) {

        setEmailOrPhoneEmailField(map.get("UserId").toString());
        setPasswordPasswordField(map.get("password").toString());
        waitABit(1000);
        return submit();
    }

    /**
     * Submit the form to target page.
     *
     * @return the FacebookLoginPage class instance.
     */
    public FacebookLoginPage submit() {
        clickLogInLink();
        FacebookLoginPage target = new FacebookLoginPage(driver, data, timeout);
        PageFactory.initElements(driver, target);
        return target;
    }


    public FacebookLoginPage() {
    }

    public FacebookLoginPage(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public FacebookLoginPage(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public FacebookLoginPage(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }


}
